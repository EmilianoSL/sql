select * from (
				select issuerCountry, count(distinct bondCode) as NumOfBonds
				from (
					SELECT  
						  STR_TO_DATE(refDate,'%d/%m/%Y') as myDate
						, STR_TO_DATE(maturityDate,'%d/%m/%Y') as ttmDate
						, DATEDIFF(STR_TO_DATE(maturityDate,'%d/%m/%Y'),
								   STR_TO_DATE(refDate,'%d/%m/%Y')
								  ) / 365.0 as ttm
						, b.*
					FROM tblBondReference b
					WHERE b.issuerCategory = 'GOVT NATIONAL'
					ORDER BY 3
					) A
				where issuerCategory = 'GOVT NATIONAL'
				AND issuerCountry <> 'WW'
				AND ttm between 0.25 and 30.0
				AND Currency = 'EUR'
				GROUP BY issuerCountry
				order by 2 desc
				LIMIT 10
        ) B
order by 1;

select sum(NumOfBonds) from (
select * from (
				select issuerCountry, count(distinct bondCode) as NumOfBonds
				from (
					SELECT  
						  STR_TO_DATE(refDate,'%d/%m/%Y') as myDate
						, STR_TO_DATE(maturityDate,'%d/%m/%Y') as ttmDate
						, DATEDIFF(STR_TO_DATE(maturityDate,'%d/%m/%Y'),
								   STR_TO_DATE(refDate,'%d/%m/%Y')
								  ) / 365.0 as ttm
						, b.*
					FROM tblBondReference b
					WHERE b.issuerCategory = 'GOVT NATIONAL'
					ORDER BY 3
					) A
				where issuerCategory = 'GOVT NATIONAL'
				AND issuerCountry <> 'WW'
				AND ttm between 0.25 and 30.0
				AND Currency = 'EUR'
				GROUP BY issuerCountry
				order by 2 desc
				LIMIT 10
        ) B
order by 1
) C;