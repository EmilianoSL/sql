
select * from vwuniquebonds;


CREATE TEMPORARY TABLE IF NOT EXISTS
tmpbondbuckets as (
				select  distinct substr(bondCode,1,2) as Country
					  , bondCode
                      , maturityDate
                      , STR_TO_DATE(refDate,'%d/%m/%Y') refDate 
                      , time2mat(refDate,maturityDate) as ttm
                      , CASE
							WHEN time2mat(refDate,maturityDate) <= 1.0  THEN '1Y'
                            WHEN time2mat(refDate,maturityDate) >  1.0  AND time2mat(refDate,maturityDate) <= 2.0  THEN '2Y'
                            WHEN time2mat(refDate,maturityDate) >  2.0  AND time2mat(refDate,maturityDate) <= 3.0  THEN '3Y'
                            WHEN time2mat(refDate,maturityDate) >  3.0  AND time2mat(refDate,maturityDate) <= 4.0  THEN '4Y'
                            WHEN time2mat(refDate,maturityDate) >  4.0  AND time2mat(refDate,maturityDate) <= 5.0  THEN '5Y'
                            WHEN time2mat(refDate,maturityDate) >  5.0  AND time2mat(refDate,maturityDate) <= 7.0  THEN '5Y-7Y'
                            WHEN time2mat(refDate,maturityDate) >  7.0  AND time2mat(refDate,maturityDate) <= 10.0 THEN '7Y-10Y'
                            WHEN time2mat(refDate,maturityDate) >  10.0 AND time2mat(refDate,maturityDate) <= 20.0 THEN '10Y-20Y'
                            WHEN time2mat(refDate,maturityDate) >  20.0 AND time2mat(refDate,maturityDate) <= 30.0 THEN '20Y-30Y'
                            WHEN time2mat(refDate,maturityDate) >  30.0 AND time2mat(refDate,maturityDate) <= 40.0 THEN '30Y-40Y'
                            ELSE '+40Y'
						END Bucket
                        , CASE
							WHEN time2mat(refDate,maturityDate) <= 1.0  THEN 1
                            WHEN time2mat(refDate,maturityDate) >  1.0  AND time2mat(refDate,maturityDate) <= 2.0  THEN 2
                            WHEN time2mat(refDate,maturityDate) >  2.0  AND time2mat(refDate,maturityDate) <= 3.0  THEN 3
                            WHEN time2mat(refDate,maturityDate) >  3.0  AND time2mat(refDate,maturityDate) <= 4.0  THEN 4
                            WHEN time2mat(refDate,maturityDate) >  4.0  AND time2mat(refDate,maturityDate) <= 5.0  THEN 5
                            WHEN time2mat(refDate,maturityDate) >  5.0  AND time2mat(refDate,maturityDate) <= 7.0  THEN 6
                            WHEN time2mat(refDate,maturityDate) >  7.0  AND time2mat(refDate,maturityDate) <= 10.0 THEN 7
                            WHEN time2mat(refDate,maturityDate) >  10.0 AND time2mat(refDate,maturityDate) <= 20.0 THEN 8
                            WHEN time2mat(refDate,maturityDate) >  20.0 AND time2mat(refDate,maturityDate) <= 30.0 THEN 9
                            WHEN time2mat(refDate,maturityDate) >  30.0 AND time2mat(refDate,maturityDate) <= 40.0 THEN 10
                            ELSE 11
						END BucketOrder
				from tblBondReference
                where issuerCategory = 'GOVT NATIONAL'
				AND issuerCountry <> 'WW'
				AND currency = 'EUR'
                );

select Country, RefDate, Bucket, count(*) as NumOfBonds
from tmpbondcodes 
where Country in ('DE','ES','FR','IT')
group by Country,refDate, Bucket
order by Country, refDate, BucketOrder
;


DROP TABLE IF EXISTS tmpbondcodes;

