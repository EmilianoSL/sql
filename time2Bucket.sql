
DELIMITER //
CREATE FUNCTION  time2Bucket( startDate varchar(12), endDate varchar(12) ) 
RETURNS varchar(12)

BEGIN
    declare bucket varchar(12);
    
    set bucket = (select 
						(CASE
							WHEN time2mat(startDate,endDate) <= 1.0  THEN '1Y'
                            WHEN time2mat(startDate,endDate) >  1.0  AND time2mat(startDate,endDate) <= 2.0  THEN '2Y'
                            WHEN time2mat(startDate,endDate) >  2.0  AND time2mat(startDate,endDate) <= 3.0  THEN '3Y'
                            WHEN time2mat(startDate,endDate) >  3.0  AND time2mat(startDate,endDate) <= 4.0  THEN '4Y'
                            WHEN time2mat(startDate,endDate) >  4.0  AND time2mat(startDate,endDate) <= 5.0  THEN '5Y'
                            WHEN time2mat(startDate,endDate) >  5.0  AND time2mat(startDate,endDate) <= 7.0  THEN '5Y-7Y'
                            WHEN time2mat(startDate,endDate) >  7.0  AND time2mat(startDate,endDate) <= 10.0 THEN '7Y-10Y'
                            WHEN time2mat(startDate,endDate) >  10.0 AND time2mat(startDate,endDate) <= 20.0 THEN '10Y-20Y'
                            WHEN time2mat(startDate,endDate) >  20.0 AND time2mat(startDate,endDate) <= 30.0 THEN '20Y-30Y'
                            WHEN time2mat(startDate,endDate) >  30.0 AND time2mat(startDate,endDate) <= 40.0 THEN '30Y-40Y'
                            WHEN time2mat(startDate,endDate) >  40.0 AND time2mat(startDate,endDate) <= 50.0 THEN '30Y-40Y'
                            ELSE '+50Y'
						END)
				  ) ;
    
	return bucket;

END; //
DELIMITER ;

	
