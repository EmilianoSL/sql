DELIMITER //
CREATE FUNCTION  time2BucketOrder( startDate varchar(12), endDate varchar(12) ) 
RETURNS int

BEGIN
    declare bucketOrder int;
    
    set bucketOrder = (select 
						(CASE
							WHEN time2mat(startDate,endDate) <= 1.0  THEN 1
                            WHEN time2mat(startDate,endDate) >  1.0  AND time2mat(startDate,endDate) <= 2.0  THEN 2
                            WHEN time2mat(startDate,endDate) >  2.0  AND time2mat(startDate,endDate) <= 3.0  THEN 3
                            WHEN time2mat(startDate,endDate) >  3.0  AND time2mat(startDate,endDate) <= 4.0  THEN 4
                            WHEN time2mat(startDate,endDate) >  4.0  AND time2mat(startDate,endDate) <= 5.0  THEN 5
                            WHEN time2mat(startDate,endDate) >  5.0  AND time2mat(startDate,endDate) <= 7.0  THEN 6
                            WHEN time2mat(startDate,endDate) >  7.0  AND time2mat(startDate,endDate) <= 10.0 THEN 7
                            WHEN time2mat(startDate,endDate) >  10.0 AND time2mat(startDate,endDate) <= 20.0 THEN 8
                            WHEN time2mat(startDate,endDate) >  20.0 AND time2mat(startDate,endDate) <= 30.0 THEN 9
                            WHEN time2mat(startDate,endDate) >  30.0 AND time2mat(startDate,endDate) <= 40.0 THEN 10
                            WHEN time2mat(startDate,endDate) >  40.0 AND time2mat(startDate,endDate) <= 50.0 THEN 11
                            ELSE 12
						END)
				  ) ;
    
	return bucketOrder;

END; //
DELIMITER ;