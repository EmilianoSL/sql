
CREATE TEMPORARY TABLE IF NOT EXISTS tmpuniquebonds(
select distinct bondCode 
from (
	  SELECT  
			  STR_TO_DATE(refDate,'%d/%m/%Y') as myDate
            , STR_TO_DATE(maturityDate,'%d/%m/%Y') as ttmDate
			, time2mat(refDate,maturityDate) as ttm
			, b.*
	  FROM tblBondReference b
	  WHERE b.issuerCategory = 'GOVT NATIONAL'
	  ORDER BY 3
	 ) A
where issuerCategory = 'GOVT NATIONAL'
AND issuerCountry <> 'WW'
AND ttm between 0.25 and 30.0
AND Currency = 'EUR'
order by 1
);

CREATE TEMPORARY TABLE IF NOT EXISTS tmpbondsdate (
select  substr(bondcode,1,2) issuerCountry 
	  , STR_TO_DATE(XDATE,'%d/%m/%Y') refDate
      , count( distinct BondCode) NumOfBonds
from tblDailyCash
where bondCode in (select * from tmpuniquebonds)
group by substr(bondcode,1,2), STR_TO_DATE(XDATE,'%d/%m/%Y')
);


select issuerCountry, sum(NumOfBonds) / count(NumOfBonds) as AvgNumOfBonds
from tmpbondsdate
group by issuerCountry
order by 2 desc
;


select * 
from (
	select substr(bondcode,1,2) issuer, count(*) UniqueBonds
	from tmpuniquebonds
	group by substr(bondcode,1,2)
	order by 2 desc
	limit 10
	) A
order by 1
;

select sum(UniqueBonds) 
from (
	select substr(bondcode,1,2) issuer, count(*) UniqueBonds
	from tmpuniquebonds
	group by substr(bondcode,1,2)
	order by 2 desc
	limit 10
	) A
order by 1
;


select *
from tmpbondsdate
where issuerCountry = 'IT'
order by 1,2;


DROP TABLE IF EXISTS tmpuniquebonds;
DROP TABLE IF EXISTS tmpbondsdate;
