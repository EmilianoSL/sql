DELIMITER //
CREATE FUNCTION  time2mat( startDate varchar(12), endDate varchar(12) ) 
RETURNS decimal(10,2)

BEGIN
	declare ttm decimal(10,2);
    
    set ttm = DATEDIFF(STR_TO_DATE(endDate,'%d/%m/%Y'),STR_TO_DATE(startDate,'%d/%m/%Y')) / 365.0;
    
	return ttm;

END; //
DELIMITER ;


