USE MTS;

CREATE OR REPLACE VIEW  vwBondReference AS

SELECT 
          DISTINCT(b.bondCode) as BondCode
        , b.bondType as BondType
        , b.description as Description
        , b.marketCode as MarketCode
        , b.issuerCountry as Country
        , b.Currency as Currency
        , CASE 
                WHEN b.DCC = 0 THEN 'ZERO_COUPON' 
                WHEN b.DCC = 1 THEN 'ACTUAL_ACTUAL'        
                WHEN b.DCC = 2 THEN 'ACTUAL_365' 
                WHEN b.DCC = 3 THEN 'ACTUAL_360' 
                WHEN b.DCC = 7 THEN 'E30_360' 
                ELSE 'ACTUAL_365' 
          END as DayCount
        , CASE 
                WHEN b.CouponFreq = 0 THEN 'ZERO' 
                WHEN b.CouponFreq = 1 THEN 'ANNUALLY'        
                WHEN b.CouponFreq = 2 THEN 'SEMIANNUALLY' 
                WHEN b.CouponFreq = 3 THEN 'QUARTERLY' 
                ELSE 'ANNUAL' 
          END as CouponFreq
         , CASE 
                WHEN b.CouponFreq = 0 THEN 'ZERO_COUPON' 
                WHEN b.CouponFreq = 1 THEN 'FIXED_COUPON'        
                WHEN b.CouponFreq = 2 THEN 'VARIABLE_COUPON' 
                WHEN b.CouponFreq = 3 THEN 'INDEXED_COUPON' 
                ELSE 'FIXED_COUPON' 
          END as CouponType
        , CASE 
			WHEN b.couponRate = '.' THEN 0.0
            ELSE convert(b.couponRate,decimal(10,2)) 
		  END as CouponRate
        , STR_TO_DATE(b.issueDate,'%d/%m/%Y') as IssueDate
        , STR_TO_DATE(b.maturityDate,'%d/%m/%Y') as MaturityDate
        , 1000000 as LotSize
        , STR_TO_DATE(b.firstSettlDate,'%d/%m/%Y') as FirstSettlDate
        , CASE 
				WHEN b.CouponFreq = 0 THEN STR_TO_DATE(b.maturityDate,'%d/%m/%Y')
                WHEN b.CouponFreq = 1 THEN STR_TO_DATE(b.lastCouponDate,'%d/%m/%Y')        
                WHEN b.CouponFreq = 2 THEN STR_TO_DATE(b.lastCouponDate,'%d/%m/%Y')
                WHEN b.CouponFreq = 3 THEN STR_TO_DATE(b.lastCouponDate,'%d/%m/%Y')
                ELSE STR_TO_DATE(b.lastCouponDate,'%d/%m/%Y')
        END as LastCouponDate
FROM tblBondReference b
WHERE b.issuerType = 'Government'
AND b.issuerCategory = 'GOVT NATIONAL'
AND b.issuerCountry in ('AT','BE','DE','ES','FI','FR','GR','IE','IT','NL','PT','SI','SK')
AND b.Currency = 'EUR'
AND b.lastCouponDate is not null
ORDER BY 1,3
;
