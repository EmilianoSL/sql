USE MTS;
CREATE OR REPLACE VIEW vwDailyCash AS
SELECT
         DISTINCT(STR_TO_DATE(c.XDATE,'%d/%m/%Y')) as RefDate
        , c.BONDCODE as BondCode
        , c.BONDTYPE as BondType
        , c.MARKETCODE as MarketCode
        , CASE 
			WHEN c.REFPRICE = '.' THEN 0.0
            ELSE C.REFPRICE
		  END as RefPrice
        , CASE 
				WHEN c.MIDYIELD = '.' OR substring(c.MIDYIELD,1,1) ='-' OR  substring(c.MIDYIELD,1,1) ='.' THEN 0.0
                ELSE c.MIDYIELD
		  END as MidYield
        , c.MAXPRICE as MaxPrice
        , c.MINPRICE as MinPrice
        , c.WGTPRICE as WgtPrice
        , CASE 
			WHEN c.ACCRUEDINTEREST = '.' THEN 0
			ELSE convert(C.ACCRUEDINTEREST,decimal(10,6))
		  END as AccruedInt
        , STR_TO_DATE(c.SETTLDATE,'%d/%m/%Y') as SettlDate
        , c.PARTNUMBER as PartNumber
        , CASE
			WHEN c.NUMOBLIGATION = '.' THEN 0
            ELSE C.NUMOBLIGATION 
		  END as NumOblg
        , convert(c.TOTVOLUME,decimal) as TotalVolume
        , convert(c.AVGSIZE,decimal) as AvgSize
        , CASE 
				WHEN c.AVGSPREAD = '.' THEN 0.0
                ELSE convert(c.AVGSPREAD,decimal(10,2))
		  END as AvgSpread	
        , convert(c.IMBTRADE,decimal) as ImbTrade
        , SUBSTRING(c.bondCode,1,2) as Country
FROM tblDailyCash c
WHERE c.wgtprice > 0
AND SUBSTRING(c.bondCode,1,2) in ('AT','BE','DE','ES','FI','FR','GR','IE','IT','NL','PT','SI','SK')
ORDER BY 1
;
